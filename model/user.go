package model

// User a user type
type User struct {
	ID       int      `pg:"id,pk" json:"id"`
	Username string   `json:"username"`
	Email    string   `json:"email"`
	Tasks    []*Task  `pg:"rel:has-many" json:"tasks"`
	Boards   []*Board `pg:"rel:has-many" json:"boards"`
}

// UserStore a collection of method for api call
type UserStore interface {
	User(id int) (User, error)
	Users() ([]User, error)
	CreateUser(u *User) error
	UpdateUser(u *User) error
	DeleteUser(id int) error
}

package model

// Board contains all tasks that belong to a user
type Board struct {
	ID          int    `pg:"id,pk" json:"id"`
	Name        string `pg:"name" json:"name"`
	Description string `pg:"desc" json:"description"`
	UserID      int    `pg:"user_id" json:"userId"`
}

type BoardStore interface {
	Board(id int) (Board, error)
	Boards() ([]Board, error)
	CreateBoard(b *Board) error
	UpdateBoard(b *Board) error
	DeleteBoard(id int) error
}

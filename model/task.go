package model

import (
	"time"
)

// TaskStatus a type that includes all available task status
type TaskStatus int

const (
	Todo TaskStatus = iota
	InProgress
	Complete
)

// Task a working tasks that belong to a board and a user
type Task struct {
	ID          int        `pg:"id,pk"`
	Name        string     `pg:"name"`
	Description string     `pg:"desc"`
	Status      TaskStatus `pg:"status"`
	User        *User      `pg:"rel:has-one"`
	UserID      int        `pg:"user_id"`
	Board       *Board     `pg:"rel:has-one"`
	BoardID     int        `pg:"board_id"`
	CreatedAt   time.Time  `pg:"created_at"`
	UpdatedAt   time.Time  `pg:"updated_at"`
}

type TaskStore interface {
	Task(id int) (Task, error)
	Tasks() ([]Task, error)
	CreateTask(b *Task) error
	UpdateTask(b *Task) error
	DeleteTask(id int) error
}

package server

import (
	"log"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/alex.tran1502/tasker/postgres"
)

// GetUser get user information with boards belong to the user
func GetUser(c *fiber.Ctx) error {

	user, err := postgres.GlobalStore.User(1)
	if err != nil {
		log.Println("err", err)
	}
	log.Println(user)
	if err != nil {
		log.Println("Cannot Get User: ", err)
		return c.Status(404).SendString("User Not Found")
	}

	log.Printf("%+v\n", user)

	c.JSON(user)

	return nil
}

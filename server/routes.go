package server

import (
	"github.com/gofiber/fiber/v2"
)

func SetupRoute(app *fiber.App) {

	// Authentication Endpoint
	app.Post("/auth", AuthHandler)

	// User
	app.Get("/api/v1/user", GetUser)

	// Boards
	app.Get("/api/v1/board", GetBoards)
	app.Get("/api/v1/board/:id", GetBoardByID)
	app.Post("/api/v1/board", AddBoard)
	app.Delete("/api/v1/board/:id", DeleteBoard)
}

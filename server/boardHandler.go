package server

import (
	"github.com/gofiber/fiber/v2"
)

// AuthHandler Authentication Hanlder
func AuthHandler(c *fiber.Ctx) error {

	c.JSON(fiber.Map{
		"status": "success",
		"method": "authenticate",
	})

	return nil
}

// AddBoard  Add a board to the database
func AddBoard(c *fiber.Ctx) error {

	c.JSON(fiber.Map{
		"status": "success",
		"method": "get",
	})
	return nil
}

// GetBoards Get all boards
func GetBoards(c *fiber.Ctx) error {

	c.JSON(fiber.Map{
		"status": "success",
		"method": "get",
	})
	return nil
}

// GetBoardByID Get board by a specific ID
func GetBoardByID(c *fiber.Ctx) error {
	c.JSON(fiber.Map{
		"status": "success",
		"method": "get",
	})
	return nil
}

// DeleteBoard Delete a board from database
func DeleteBoard(c *fiber.Ctx) error {

	c.JSON(fiber.Map{
		"status": "success",
		"method": "get",
	})
	return nil
}

module gitlab.com/alex.tran1502/tasker

go 1.15

require (
	github.com/cosmtrek/air v1.21.2 // indirect
	github.com/creack/pty v1.1.11 // indirect
	github.com/fatih/color v1.10.0 // indirect
	github.com/go-pg/pg/v10 v10.5.1
	github.com/gofiber/fiber/v2 v2.1.3
	github.com/imdario/mergo v0.3.11 // indirect
	github.com/pelletier/go-toml v1.8.1 // indirect
	golang.org/x/sys v0.0.0-20201113233024-12cec1faf1ba // indirect
	google.golang.org/appengine v1.6.7
)

package main

import (
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/limiter"
	"gitlab.com/alex.tran1502/tasker/postgres"
	"gitlab.com/alex.tran1502/tasker/server"
)

func main() {
	db, err := postgres.InitDatabase()
	if err != nil {
		panic("Cannot established Connection to Database")
	}
	defer db.Close()

	// Initialize Store
	err = postgres.NewStore(db)

	if err != nil {
		panic("Setup Store Failed")
	}

	app := fiber.New()

	// Setup Limiter Middle Ware
	app.Use(limiter.New(limiter.Config{
		Expiration: 10 * time.Second,
	}))

	// Setup Route
	server.SetupRoute(app)

	// Server Webapp
	app.Static("/", "./client/build")
	_ = app.Listen("localhost:8080")

}

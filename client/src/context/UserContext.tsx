import React from 'react';

type Board = {
  id: number;
  name: string;
  description: string;
  userID: number;
};

type UserType = {
  id: number;
  username: string;
  email: string;
  isAuth: boolean;
  boards: Board[];
};

const defaultUser: UserType = {
  id: 0,
  username: '',
  email: '',
  isAuth: false,
  boards: [],
};

type UserContextType = {
  user: UserType;
  setUser: (user: UserType) => void;
};

export const UserContext = React.createContext<UserContextType | undefined>(
  undefined
);

type Props = {
  children: React.ReactNode;
};

export const UserProvider = ({ children }: Props) => {
  const [user, setUser] = React.useState(defaultUser);
  // const value = React.useMemo(() => [user, setUser], [user]);

  return (
    <UserContext.Provider value={{ user, setUser }}>
      {children}
    </UserContext.Provider>
  );
};

export const useUser = () => {
  const { user, setUser } = React.useContext(UserContext)!;

  const getUser = async () => {
    try {
      const r = await fetch('/api/v1/user');
      const userInfo = await r.json();
      console.log(userInfo);
      setUser({
        ...user,
        id: userInfo.id,
        username: userInfo.username,
        email: userInfo.email,
        isAuth: false,
        boards: userInfo.boards,
      });
    } catch (e) {
      console.warn('Failed To Get User ', e);
    }
  };
  return { user, getUser };
};

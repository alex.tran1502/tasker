import './App.scss';
import NavBar from './components/NavBar';
import { Route, Switch } from 'react-router-dom';
import Boards from './views/Boards';
import Home from './views/Home';
import React from 'react';
import TabController from './components/TabController';
import { UserProvider } from './context/UserContext';

function App() {
  return (
    <div className="App">
      <UserProvider>
        <header className="App-header">
          <NavBar />
          <TabController />
        </header>

        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/boards">
            <Boards />
          </Route>
        </Switch>
      </UserProvider>
    </div>
  );
}

export default App;

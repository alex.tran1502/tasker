import React from 'react';
import { useUser } from '../context/UserContext';

const Home = () => {
  const { user } = useUser();
  return (
    <>
      <h1>This is the home page</h1>
      {user.boards.map((board) => {
        console.log(board);
        return <h1 key={board.id}>{board.name}</h1>;
      })}
    </>
  );
};

export default Home;

import React from 'react';
import styles from './TabController.module.scss';
import { BiFolderPlus } from 'react-icons/bi';
function TabController() {
  return (
    <>
      <section className={styles.TabControllerWrapper}>
        <div>Home</div>
        <div className={styles.BoardListWrapper}>
          <ul>
            <li>Tasker</li>
            <li>Marsubot</li>
          </ul>
        </div>
        <div>
          <button className={styles.AddBoardButton} >
            <span>
              <BiFolderPlus size={17} />
            </span>
            <span>New Board</span>
          </button>
        </div>
      </section>
    </>
  );
}

export default TabController;

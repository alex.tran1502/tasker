import * as React from 'react';
import styles from './DropdownButton.module.scss';
import { BiChevronDown, BiChalkboard } from 'react-icons/bi';

type DropDownButtonProps = {
  children: string[];
  color?: string;
  onClick: () => void;
};

const DropdownButton: React.FC<DropDownButtonProps> = ({
  children,
  color,
  onClick,
}) => {
  const iconSize = 15;
  return (
    <>
      <button className={styles.DropdownButton} onClick={onClick}>
        <span>
          <BiChalkboard size={iconSize} />
        </span>
        <span>Choose A Board </span>
        <span>
          <BiChevronDown size={iconSize} />
        </span>
      </button>
    </>
  );
};

export default DropdownButton;

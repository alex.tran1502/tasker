import style from './NavBar.module.scss';
import React from 'react';
import { useUser } from '../context/UserContext';

const NavBar = () => {
  const { user, getUser } = useUser()!;

  React.useEffect(() => {
    getUser();
  }, []);

  return (
    <>
      <div className={style.NavbarWrapper}>
        <h3>Tasker</h3>
        <div>
          <h1>Hello {user.username}</h1>
          <p>You have {user.boards.length} Boards and 0 in-progress task</p>
        </div>
        <div>
          <h4>Setting</h4>
          <p>Alex. T</p>
        </div>
      </div>
    </>
  );
};

export default NavBar;

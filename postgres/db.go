package postgres

import (
	"context"
	"log"

	"github.com/go-pg/pg/v10"
)

// DBConn Database Instance
var DBConn *pg.DB

//InitDatabase Initialize Database Connection and return an instance
func InitDatabase() (*pg.DB, error) {
	opt, err := pg.ParseURL("postgres://postgres:postgres@localhost:5432/tasker?sslmode=disable")

	if err != nil {
		panic(err)
	}

	DBConn = pg.Connect(opt)

	ctx := context.Background()

	if err := DBConn.Ping(ctx); err != nil {
		panic(err)
	}

	log.Println("Init database instance")

	return DBConn, nil
}

package postgres

import (
	"github.com/go-pg/pg/v10"
	"gitlab.com/alex.tran1502/tasker/model"
)

type BoardStore struct {
	*pg.DB
}

func (s *BoardStore) Board(id int) (model.Board, error) {
	var board model.Board

	err := s.DB.Model(&board).Where("id = ?", id).Select()
	if err != nil {
		return model.Board{}, err
	}

	return board, nil
}

func (s *BoardStore) Boards() ([]model.Board, error) {
	var bb []model.Board
	err := s.DB.Model(&bb).Select()
	if err != nil {
		return nil, err
	}

	return bb, nil
}

func (s *BoardStore) CreateBoard(b *model.Board) error {
	_, err := s.DB.Model(&b).Insert()
	if err != nil {
		return err
	}

	return nil
}

func (s *BoardStore) UpdateBoard(b *model.Board) error {
	_, err := s.DB.Model(&b).WherePK().Update()
	if err != nil {
		return err
	}

	return nil
}

func (s *BoardStore) DeleteBoard(id int) error {
	var board model.Board
	_, err := s.DB.Model(&board).Where("id = ?", id).Delete()
	if err != nil {
		return err
	}

	return nil
}

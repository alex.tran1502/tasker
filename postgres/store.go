package postgres

import (
	"fmt"
	"log"

	"github.com/go-pg/pg/v10"
)

var GlobalStore *Store

// Store include all stores, convenient for dependency injection
type Store struct {
	*UserStore
	*TaskStore
	*BoardStore
}

func NewStore(db *pg.DB) error {
	if err := db.Ping(db.Context()); err != nil {
		return fmt.Errorf("Error connecting to database: %w", err)
	}

	log.Println("Init New Store")
	GlobalStore = &Store{
		UserStore:  &UserStore{DB: db},
		BoardStore: &BoardStore{DB: db},
		TaskStore:  &TaskStore{DB: db},
	}

	return nil
}

package postgres

import (
	"github.com/go-pg/pg/v10"
	"gitlab.com/alex.tran1502/tasker/model"
)

type TaskStore struct {
	*pg.DB
}

func (s *TaskStore) Task(id int) (model.Task, error) {
	var task model.Task

	err := s.DB.Model(&task).Where("id = ?", id).Select()
	if err != nil {
		return model.Task{}, err
	}

	return task, nil

}

func (s *TaskStore) Tasks() ([]model.Task, error) {
	var tt []model.Task
	err := s.DB.Model(&tt).Select()
	if err != nil {
		return nil, err
	}

	return tt, nil
}

func (s *TaskStore) CreateTask(t *model.Task) error {
	_, err := s.DB.Model(&t).Insert()
	if err != nil {
		return err
	}

	return nil
}

func (s *TaskStore) UpdateTask(t *model.Task) error {
	_, err := s.DB.Model(&t).WherePK().Update()
	if err != nil {
		return err
	}

	return nil
}

func (s *TaskStore) DeleteTask(id int) error {
	var task model.Task
	_, err := s.DB.Model(&task).Where("id = ?", id).Delete()
	if err != nil {
		return err
	}

	return nil
}

package postgres

import (
	"log"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"gitlab.com/alex.tran1502/tasker/model"
)

type UserStore struct {
	*pg.DB
}

func (s *UserStore) User(id int) (model.User, error) {
	log.Println("Get User with id", id)
	var user model.User

	err := s.DB.Model(&user).
		Where("id = ?", id).
		Relation("Boards", func(q *orm.Query) (*orm.Query, error) {
			return q.Where("user_id = 1"), nil
		}).
		Select()
	if err != nil {
		return model.User{}, err
	}

	return user, nil

}

func (s *UserStore) Users() ([]model.User, error) {
	var uu []model.User
	err := s.DB.Model(&uu).Select()
	if err != nil {
		return nil, err
	}

	return uu, nil
}

func (s *UserStore) CreateUser(u *model.User) error {
	_, err := s.DB.Model(&u).Insert()
	if err != nil {
		return err
	}

	return nil
}

func (s *UserStore) UpdateUser(u *model.User) error {
	_, err := s.DB.Model(&u).WherePK().Update()
	if err != nil {
		return err
	}

	return nil
}

func (s *UserStore) DeleteUser(id int) error {
	var user model.User
	_, err := s.DB.Model(&user).Where("id = ?", id).Delete()
	if err != nil {
		return err
	}

	return nil
}

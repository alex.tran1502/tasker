.PHONY: migrateup

migrateup:
	migrate -path migrations/ -database postgres://postgres:postgres@localhost/tasker?sslmode=disable up

migratedown:
	migrate -path migrations/ -database postgres://postgres:postgres@localhost/tasker?sslmode=disable down

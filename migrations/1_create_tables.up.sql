CREATE TABLE users (
	id SERIAL PRIMARY KEY,
	username TEXT NOT NULL,
	email TEXT NOT NULL
);

CREATE TABLE boards (
	id SERIAL PRIMARY KEY,
	"name" TEXT NOT NULL,
	"desc" TEXT NOT NULL,
	user_id SERIAL NOT NULL REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE tasks (
	id SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL,
	"desc" TEXT NOT NULL,
	status INT DEFAULT 0,
	user_id SERIAL NOT NULL REFERENCES users(id) ON DELETE CASCADE,
	board_id SERIAL NOT NULL REFERENCES boards(id) ON DELETE CASCADE,
	created_at timestamptz NOT NULL,
	updated_at timestamptz NOT NULL
);

-- Example Data
INSERT INTO users (id, username, email) VALUES (1, 'alex.tran', 'alex.tran1502@gmail.com');
INSERT INTO boards (id, name, "desc", user_id) VALUES (1, 'Build Casio Clock', 'Build a casio clock to make home nice', 1);
INSERT INTO boards (id, name, "desc", user_id) VALUES (2, 'Drive F1 Fomular Car', 'A journey to get to drive a real F1 car', 1);